package com.prueba.dacodes;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.SearchView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.prueba.dacodes.api.Callback;
import com.prueba.dacodes.constant.Constant;
import com.prueba.dacodes.databinding.ActivityMainBinding;
import com.prueba.dacodes.model.CocktailModel;
import com.prueba.dacodes.ui.adapter.CocktailAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static com.prueba.dacodes.api.ApiRequest.Call_Api;

public class MainActivity extends AppCompatActivity {
    ActivityMainBinding binding;
    private CocktailAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(binding.toolbar);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        binding.contentMain.rvCocktails.setLayoutManager(manager);
        handleIntent(getIntent());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        if(searchManager != null)
            searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    private void Search(String searchStr){
        String url = Constant.COCKTAIL_API + searchStr;
        Call_Api(MainActivity.this, url, null, new Callback() {
            @Override
            public void Response(String resp) {
                try{
                    JSONObject jsonResponse = new JSONObject(resp);
                    if(jsonResponse.has("drinks")){
                        JSONArray array = jsonResponse.getJSONArray("drinks");
                        Gson gson = new Gson();
                        List<CocktailModel> cocktailModels = gson.fromJson(array.toString(), new TypeToken<List<CocktailModel>>(){}.getType());
                        adapter = new CocktailAdapter(cocktailModels);
                        binding.contentMain.rvCocktails.setAdapter(adapter);
                        binding.contentMain.emptyResults.setVisibility(View.GONE);
                    }
                }
                catch(JSONException ex){
                    binding.contentMain.emptyResults.setVisibility(View.VISIBLE);
                    ex.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            //use the query to search your data somehow
            Search(query);
        }else{
            Search("margarita");
        }
    }
}
