package com.prueba.dacodes.ui.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.prueba.dacodes.databinding.ItemCocktailBinding;
import com.prueba.dacodes.model.CocktailModel;

import java.util.List;

public class CocktailAdapter extends RecyclerView.Adapter<CocktailAdapter.CocktailViewHolder> {
    private List<CocktailModel> cocktails;
    private OnItemSelectedListener listener;
    // Provide a suitable constructor (depends on the kind of dataset)
    public CocktailAdapter(List<CocktailModel> cocktails) {
        this.cocktails = cocktails;
    }

    public void setOnItemSelectedListener(OnItemSelectedListener listener){
        this.listener = listener;
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public CocktailAdapter.CocktailViewHolder onCreateViewHolder( ViewGroup parent,
                                                              int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ItemCocktailBinding itemBinding = ItemCocktailBinding.inflate(layoutInflater, parent, false);
        return new CocktailViewHolder(itemBinding);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(CocktailViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final CocktailModel coctail = this.cocktails.get(position);
        //person.setDescription(person.getDescription().substring(0, 100));
        Glide.with(holder.itemView.getContext()).load(coctail.getPicture()).into(holder.binding.pic);
        holder.bind(coctail);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return cocktails.size();
    }

    public CocktailModel getItem(int position){
        return cocktails.get(position);
    }

    class CocktailViewHolder extends RecyclerView.ViewHolder {
        private ItemCocktailBinding binding;

        private CocktailViewHolder(ItemCocktailBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        private void bind(CocktailModel person) {
            binding.setItem(person);
            binding.executePendingBindings();
        }
    }

    public interface OnItemSelectedListener {
        void onItemSelected(CocktailModel cocktail);
    }
}
