package com.prueba.dacodes.model;

import com.google.gson.annotations.SerializedName;

public class CocktailModel {
    @SerializedName("strDrinkThumb") private String Picture;
    @SerializedName("strDrink") private String Name;
    @SerializedName("strInstructions") private String Description;

    public String getPicture() {
        return Picture;
    }

    public void setPicture(String picture) {
        Picture = picture;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }
}
